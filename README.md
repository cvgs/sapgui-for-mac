SAPGUI Package
==============

About
-----

This package installs the SAP GUI for Mac and the configuration files for the SAP backend.

There are makefiles two packages:

 - `SAPGUI-7.5.0.8.0.pkg`: This is the package for the application itself, it does not contain any site-specific information.
 - `SAPGUI_Config_PretendCo-1.0.0.pkg`: This is a package containing client-specific files that are installed into `/Library/Preferences/SAP`. You can edit the Makefile and change `PretendCo` as you wish (like `YourCompany_Accounting` or really anything you like).


Building the package
--------

This is a package created by "luggage", so you need to install that first from <https://github.com/unixorn/luggage>.

To install luggage, simply use the following steps:

	cd /tmp
	git clone https://github.com/unixorn/luggage.git
	cd luggage
	sudo make bootstrap_files

Building the package is a multi-step process:


### Step 1: Configuration Template Files

On first start, the SAP GUI will copy configuration templates into the users SAP preferences directory. We provide a default configuration templates in the directory `SAPGUI_Templates` which does not need to be changed. This file simply references configuration files at the system-wide location `/Library/Preferences/SAP` (if you would like to use other locations, you need to create a `SAPGUI.policy.template` and define the rules there).

You can put your own configuration files into `SAPGUI_Preferences` and configure those accordingly. This allows you to overwrite those at a later stage, if needed.
You will at least need a file named `settings` and probably a `trustClassification` (to configure trust levels for your backend systems).
You can also use the `SAPUILANDSCAPE=...` parameter to add `SAPGUILandscape.xml` from the same directory, or include it using an HTTPS URL. 

### Step 2: Configure TLS certificates

If you use TLS for central configuration files, you need Java to recognize your internal CAs. Instead of adding your
CAs to the java keystore, we reconfigure the SAPGUI JRE to use the macOS keychain as trustStore.

We simply add the following options to the `Info.plist` file (usually at `/Applications/SAP Clients/SAPGUI 7.50rev8/SAPGUI 7.50rev8.app/Contents/Info.plist`). The `[...]` show already existing entries which you should ignore.

	<key>SAPJava</key>
	<dict>
		[...]
		<key>Properties</key>
		<dict>
			[...]
 			<key>javax.net.ssl.trustStoreProvider</key>
			<string>Apple</string>
 			<key>javax.net.ssl.trustStoreType</key>
			<string>KeychainStore</string>
		</dict>
	</dict>

### Step 2: Additions for SSO

To get SSO to work, simply install the `SecureLoginClient.pkg` package from SAP. Drawback of this is an additional menu bar icon (blue checkmark) which cannot be removed.

### Step 3: Install SAPGUI on build machine

On the build machine, you will cleanly install SAPGUI using the jar file provided by SAP. You will need to *have Java installed* for this step to work. You also might need to uninstall any previously installed version of the SAP GUI.

Copy the vendor GUI installer (named like `PlatinGUI750_8-80002494.JAR`) into `Vendor Downloads`, and then simply issue `sudo make sapzip`.

### Step 4: Create the package

Then you can simple use `sudo make pkg` to create the final package (like `SAPGUI-7.5.0.8.0.pkg`). This will take care of proper permissions, locations and folder structures.


Distribution
------------

Simply deploy the resulting pkg, there are no prerequisites. The configuration files will be installed on first launch of the SAPGUI application into the user's preference folder.
